OpenKAT op Haven
---


# Development

1. `minikube$ helm install openkat ./openkat -n openkat --create-namespace`

# Generate .env file

Via https://github.com/minvws/nl-kat-coordination/blob/main/docs/containers.md#setup

```shell
git clone https://github.com/minvws/nl-kat-coordination.git
cd nl-kat-coordination
make env
cd ..
cp ./nl-kat-coordination/.env ./.env
```
